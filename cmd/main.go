package main

import (
	"gitlab.com/AHerczeg/3rdvision/internal/controller"
)

func main() {
	controller.Run()
}
