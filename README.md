# Requirements
* [SDL2](http://libsdl.org/download-2.0.php)
  * ```apt install libsdl2-dev```
* [PortAudio](http://www.portaudio.com/)
  * ```apt install portaudio19-dev```

# Usage
First connect an audio source (e.g. microphone) to your machine
then run the program with
```make run```

For optional flags, run:
```
make build
./3rdvision <FLAGS>
```

# Flags
* ```-d``` run the program with pre-recorded dummy data instead of using an audio input
* ```-v``` select an audio renderer
  * ```MLESNIAK``` A simple oscilloscope based on the work of [Michael Lesniak](https://github.com/mlesniak)
  * ```NEOMLESNIAK``` A reworked version of ```MLESNIAK```
  * ```FLOW```  A continuously flowing oscilloscope ([see](https://en.wikipedia.org/wiki/Unknown_Pleasures))