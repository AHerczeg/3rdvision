package visualiser

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

type mlesniakVisualiser struct {
	window   *sdl.Window
	renderer *sdl.Renderer
}

func initMlesniak() (*mlesniakVisualiser, error) {
	window, renderer, err := initSDL()
	if err != nil {
		return nil, fmt.Errorf("failed to initialise SDL: %w", err)
	}
	mlesniak := mlesniakVisualiser{window: window, renderer: renderer}

	return &mlesniak, nil
}

func (m *mlesniakVisualiser) close() {
	m.window.Destroy()
	m.renderer.Destroy()
	sdl.Quit()
}

func (m *mlesniakVisualiser) draw(buffer *[]int32) {
	m.drawVoice(*buffer)
}

func (m *mlesniakVisualiser) drawVoice(buffer []int32) {
	m.renderer.SetDrawColor(0, 0, 0, 255)
	m.renderer.Clear()
	m.renderer.SetDrawColor(255, 255, 255, 255)
	for i := range buffer {
		x := int(float32(i) * widthFactor)
		y := int32(float32(buffer[x])*heightFactor + height/2)
		m.renderer.DrawPoint(int32(x), y)
	}
	m.renderer.Present()
}

func (m *mlesniakVisualiser) CheckExitSignal() bool {
	var event sdl.Event
	for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			return true
		case *sdl.KeyboardEvent:
			if t.Keysym.Sym == 27 {
				// Quit on Escape key.
				return true
			}
		}
	}
	return false
}
