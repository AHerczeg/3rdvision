package visualiser

import (
	"container/list"
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

type flowRenderer struct {
	window   *sdl.Window
	renderer *sdl.Renderer
	queue    *list.List
}

func initFlowRenderer() (*flowRenderer, error) {
	window, renderer, err := initSDL()
	if err != nil {
		return nil, fmt.Errorf("failed to initialise SDL: %w", err)
	}

	flowRenderer := flowRenderer{window: window, renderer: renderer, queue: list.New()}

	return &flowRenderer, nil
}

func (f *flowRenderer) close() {
	f.window.Destroy()
	f.renderer.Destroy()
	sdl.Quit()
}

func (f *flowRenderer) draw(buffer *[]int32) {
	f.processBuffer(*buffer)
	f.drawVoice()
}

func (f *flowRenderer) processBuffer(buffer []int32) {
	maxSize := 254
	if f.queue.Len() >= maxSize {
		f.queue.Remove(f.queue.Back())
	}
	tmp := make([]int32, len(buffer))
	copy(tmp, buffer)

	f.queue.PushFront(tmp)

	e := f.queue.Back()
	for e != nil {
		e = e.Prev()
	}
}

func (f *flowRenderer) drawVoice() {
	f.renderer.SetDrawColor(0, 0, 0, 255)
	f.renderer.Clear()
	f.renderer.SetDrawColor(255, 255, 255, 255)
	var offset int32
	e := f.queue.Front()
	for e != nil {
		raw := e.Value.([]int32)
		points := createPoints(raw, offset)
		if points != nil {
			f.renderer.DrawLines(points)
		}
		e = e.Next()
		offset++
		var alpha uint8
		if 255-(offset*2) > 0 {
			alpha = uint8(255 - (offset * 2))
		}
		f.renderer.SetDrawColor(255, 255, 255, alpha)
	}
	f.renderer.Present()
}

func createPoints(buffer []int32, offset int32) []sdl.Point {
	var points []sdl.Point
	for i := range buffer {
		x := int32(float32(i) * widthFactor)
		y := int32((float32(buffer[x])*heightFactor/10 + height)) - (offset * 7)

		if isInBounds(x, y) {
			points = append(points, sdl.Point{X: x, Y: y})
		}
	}
	return points
}

func isInBounds(x, y int32) bool {
	if x < 0 || x > width || y < 0 || y > height {
		return false
	}
	return true
}

func (f *flowRenderer) CheckExitSignal() bool {
	var event sdl.Event
	for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			return true
		case *sdl.KeyboardEvent:
			if t.Keysym.Sym == 27 {
				// Quit on Escape key.
				return true
			}
		}
	}
	return false
}

// TODO
func addElement(l *list.List, element []int) {
	maxSize := 4
	if l.Len() >= maxSize {
		l.Remove(l.Back())
	}
	l.PushFront(element)

}

// TODO
func printQueue(queue *list.List) {
	e := queue.Back()
	for e != nil {
		e = e.Prev()
	}
}
