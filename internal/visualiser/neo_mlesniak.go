package visualiser

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

type neoMlesniak struct {
	window        *sdl.Window
	renderer      *sdl.Renderer
	numberOfLines int
	lineThickness int
}

func initNeoMlesniak() (*neoMlesniak, error) {
	window, renderer, err := initSDL()
	if err != nil {
		return nil, fmt.Errorf("failed to initialise SDL: %w", err)
	}

	neoMlesniak := neoMlesniak{window: window, renderer: renderer, lineThickness: 5}

	return &neoMlesniak, nil
}

func (m *neoMlesniak) close() {
	m.window.Destroy()
	m.renderer.Destroy()
	sdl.Quit()
}

func (m *neoMlesniak) draw(buffer *[]int32) {
	m.drawVoice(*buffer)
}

func (m *neoMlesniak) drawVoice(buffer []int32) {
	m.renderer.SetDrawColor(1, 1, 1, 1)
	m.renderer.Clear()
	m.renderer.SetDrawColor(255, 255, 255, 255)
	var points []sdl.Point
	for i := range buffer {
		x := int32(float32(i) * widthFactor)
		y := int32(float32(buffer[x])*heightFactor + height/2)
		points = append(points, sdl.Point{X: x, Y: y})
	}
	m.renderer.DrawLines(points)
	m.renderer.Present()
}

func (m *neoMlesniak) CheckExitSignal() bool {
	var event sdl.Event
	for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			return true
		case *sdl.KeyboardEvent:
			if t.Keysym.Sym == 27 {
				// Quit on Escape key.
				return true
			}
		}
	}
	return false
}
