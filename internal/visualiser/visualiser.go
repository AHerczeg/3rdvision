package visualiser

import (
	"container/ring"
	"context"
	"fmt"
	"time"
)

const (
	bufferSize = 1024

	refreshRateMicro = 100000

	width  = 800
	height = 800

	// Factors to shrink the measured data with large integers into the window.
	widthFactor  = width / float32(1024)
	maxAmplitude = 500000000
	heightFactor = height / float32(maxAmplitude)

	flowRendererType        = "FLOW"
	mlesniakRendererType    = "MLESNIAK"
	neomlesniakRendererType = "NEOMLESNIAK"
)

type visualiser interface {
	draw(buffer *[]int32)
	close()
}

func Run(ctx context.Context, typeOfVisualiser string) (chan<- int32, error) {
	renderer, err := initVisualiser(typeOfVisualiser)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return readStream(ctx, renderer), nil
}

func readStream(ctx context.Context, vis visualiser) chan<- int32 {
	stream := make(chan int32, bufferSize)

	ticker := time.NewTicker(refreshRateMicro * time.Microsecond)
	buffer := ring.New(bufferSize)
	var input int32

	go func() {
		for {
			select {
			case <-ctx.Done():
				vis.close()
				return
			case input = <-stream:
				buffer.Value = input
				buffer = buffer.Next()
			case <-ticker.C:
				vis.draw(ringToSlice(buffer))
			}
		}
	}()

	return stream
}

func ringToSlice(r *ring.Ring) *[]int32 {
	var buffer []int32
	r.Do(func(i interface{}) {
		if i == nil {
			buffer = append(buffer, 0)
			return
		}
		buffer = append(buffer, i.(int32))
	})
	return &buffer
}

func initVisualiser(typeOfVisualiser string) (visualiser, error) {
	switch typeOfVisualiser {
	case flowRendererType:
		return initFlowRenderer()
	case mlesniakRendererType:
		return initMlesniak()
	case neomlesniakRendererType:
		return initNeoMlesniak()
	default:
		return nil, fmt.Errorf("render type not recognised")
	}
}
