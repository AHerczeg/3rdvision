package visualiser

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

func initSDL() (*sdl.Window, *sdl.Renderer, error) {
	// Initialize graphics part.
	if err := sdl.Init(sdl.INIT_VIDEO); err != nil {
		return nil, nil, fmt.Errorf("failed to initialies SDL: %w", err)
	}

	window, err := sdl.CreateWindow("3rd Vision", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		width, height, sdl.WINDOW_SHOWN)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create window: %w", err)
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		window.Destroy()
		return nil, nil, fmt.Errorf("failed to create renderer: %w", err)
	}

	// Set blend mode to BLENDMODE_BLEND
	renderer.SetDrawBlendMode(1)

	return window, renderer, nil
}
