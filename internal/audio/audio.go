package audio

import (
	"fmt"
	"time"

	"github.com/gordonklaus/portaudio"
)

const (
	sampleRate = 44100
	scanTicker = 100 * time.Microsecond
	bufferSize = 1024
)

type manager interface {
	close() error
	start() error
	read() error
	stop() error
}

type portaudioManager struct {
	stream *portaudio.Stream
}

func initManager(buffer []int32) (*portaudioManager, error) {
	// Initialize audio library.
	if err := portaudio.Initialize(); err != nil {
		return nil, fmt.Errorf("failed to initialis portaudio: %w", err)
	}
	stream, err := portaudio.OpenDefaultStream(1, 0, sampleRate, len(buffer), buffer)
	if err != nil {
		return nil, fmt.Errorf("failed to open portaudio streawm: %w", err)
	}

	return &portaudioManager{stream: stream}, nil
}

func (m *portaudioManager) close() error {
	if err := m.stream.Close(); err != nil {
		return fmt.Errorf("failed to close portaudio stream: %w", err)
	}

	if err := portaudio.Terminate(); err != nil {
		return fmt.Errorf("failed to terminate portaudio: %w", err)
	}

	return nil
}

func (m *portaudioManager) start() error {
	return m.stream.Start()
}

func (m *portaudioManager) read() error {
	return m.stream.Read()
}

func (m *portaudioManager) stop() error {
	return m.stream.Stop()
}

type DummyManager struct {
	samples   [][]int32
	buffer    []int32
	nextSlice int
}

func initDummy(buffer []int32) *DummyManager {
	return &DummyManager{samples: Samples(), buffer: buffer, nextSlice: 0}
}

func (m *DummyManager) close() error {
	return nil
}

func (m *DummyManager) start() error {
	return nil
}

func (m *DummyManager) read() error {
	return m.readSample()
}

func (m *DummyManager) stop() error {
	return nil
}

func (m *DummyManager) readSample() error {
	if len(m.samples) == 0 {
		return fmt.Errorf("test sample is empty")
	}

	if m.nextSlice > len(m.samples)-1 {
		m.nextSlice = 0
	}

	copy(m.buffer, m.samples[m.nextSlice])

	m.nextSlice++

	return nil
}
