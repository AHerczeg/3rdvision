package audio

import (
	"container/ring"
	"context"
)

func readStream(ctx context.Context) chan<- int32 {
	stream := make(chan int32, bufferSize)
	minOutput, maxOutput, avgOutput := make(chan int32), make(chan int32), make(chan int32)
	buffer := ring.New(bufferSize)
	var input int32
	var min, max, avg int32

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case input = <-stream:
				buffer.Value = input
				buffer = buffer.Next()
			case minOutput <- min:
			case maxOutput <- max:
			case avgOutput <- avg:
			default:
				min = minInRing(buffer)
				max = maxInRing(buffer)
				avg = avgInRing(buffer)
			}
		}
	}()

	return stream
}

func maxInRing(r *ring.Ring) int32 {
	max := r.Value.(int32)
	r.Do(func(i interface{}) {
		if i.(int32) > max {
			max = i.(int32)
		}
	})
	return max
}

func minInRing(r *ring.Ring) int32 {
	min := r.Value.(int32)
	r.Do(func(i interface{}) {
		if i.(int32) < min {
			min = i.(int32)
		}
	})
	return min
}

func avgInRing(r *ring.Ring) int32 {
	var sum int32
	r.Do(func(i interface{}) {
		sum += i.(int32)
	})
	return sum / int32(r.Len())
}
