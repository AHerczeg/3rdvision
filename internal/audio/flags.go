package audio

import "flag"

var useSample = flag.Bool("d", false, "When set to true, use pre-recorded audio as input")
