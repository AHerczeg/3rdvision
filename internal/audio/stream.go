package audio

import (
	"context"
	"fmt"
	"time"
)

type Streamer interface {
}

type portaudioStream struct {
	ctx       context.Context
	err       chan error
	source    manager
	consumers []chan<- int32
}

func Run(ctx context.Context) (func(chan<- int32), <-chan error) {
	p := portaudioStream{ctx, make(chan error, 1), nil, []chan<- int32{}}
	go p.run()
	return p.add, p.err
}

func (p *portaudioStream) run() {
	readBuffer := make([]int32, bufferSize)
	sendBuffer := make([]int32, bufferSize)
	ticker := time.NewTicker(scanTicker)
	var index int
	if err := p.initSource(readBuffer); err != nil {
		p.err <- err
		return
	}
	defer func() {
		if err := p.source.close(); err != nil {
			p.err <- err
		}
	}()
	if err := p.source.start(); err != nil {
		p.err <- err
		return
	}
	for {
		select {
		case <-p.ctx.Done():
			if err := p.close(); err != nil {
				p.err <- err
			}
			return
		case <-ticker.C:
			if err := p.source.read(); err != nil {
				p.err <- err
				return
			}
			copy(sendBuffer, readBuffer)
			index = 0
		default:
			if index > len(sendBuffer)-1 {
				continue
			}
			for _, c := range p.consumers {
				select {
				case c <- sendBuffer[index]:
				default:
				}
			}
			index++
		}
	}
}

func (p *portaudioStream) initSource(buffer []int32) error {
	if *useSample {
		p.source = initDummy(buffer)
	} else {
		var err error
		p.source, err = initManager(buffer)
		if err != nil {
			return fmt.Errorf("failed to start audio manager: %w", err)
		}
	}

	return nil
}

func (p *portaudioStream) close() error {
	return p.source.close()
}

func (p *portaudioStream) add(consumer chan<- int32) {
	p.consumers = append(p.consumers, consumer)
}
