package controller

import "flag"

var typeOfVisualiser = flag.String("v", "", "Type of visualiser to use")
