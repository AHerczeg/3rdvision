package controller

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/AHerczeg/3rdvision/internal/audio"
	"gitlab.com/AHerczeg/3rdvision/internal/visualiser"
	"os"
	"os/signal"
	"time"
)

const timeout = 1 * time.Second

func Run() {
	flag.Parse()
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	addStreamListener, audioError := audio.Run(ctx)

	visualiserInput, err := visualiser.Run(ctx, *typeOfVisualiser)
	if err != nil {
		fmt.Println(err)
		return
	}

	addStreamListener(visualiserInput)

	for {
		select {
		case errReceived := <-audioError:
			fmt.Println(errReceived)
			cancel()
			time.Sleep(timeout)
			return
		case <-sig:
			cancel()
			time.Sleep(timeout)
			return
		}
	}
}
