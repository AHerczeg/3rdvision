.PHONY: lint
lint:
	go fmt ./...
	go vet ./...

.PHONY: clean
clean:
	rm ./3rdvision

.PHONY: build
build: lint           ## Builds the go binary.
	go build -o 3rdvision ./cmd

.PHONY: run
run: build           ## Builds and runs the go binary with the FLOW visualiser.
	./3rdvision -v FLOW

.PHONY: run_sample
sample: build           ## Builds and runs the go binary with the FLOW visualiser on pre-recorded sample data.
	./3rdvision -d -v FLOW


