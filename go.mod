module gitlab.com/AHerczeg/3rdvision

go 1.13

require (
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/veandco/go-sdl2 v0.4.1
)
